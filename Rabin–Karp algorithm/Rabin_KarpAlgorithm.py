def rabin_karp(text, pattern):
    n = len(text)
    m = len(pattern)
    pattern_hash = hash(pattern)
    text_hash = hash(text[:m])
    for i in range(n - m + 1):
        if text_hash == pattern_hash:
            if text[i:i+m] == pattern:
                return i
        if i < n - m:
            text_hash = rolling_hash(text_hash, text[i], text[i+m])
    return -1

def hash(string):
    h = 0
    for c in string:
        h = (h * 31 + ord(c)) % (2**32)
    return h

def rolling_hash(hash_value, old_char, new_char):
    return (hash_value * 31 - ord(old_char) * 31**(m-1) + ord(new_char)) % (2**32)
